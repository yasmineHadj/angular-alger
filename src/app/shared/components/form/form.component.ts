import { Component, OnInit } from '@angular/core';
import { State } from '../../enums/state.enum';
import { Item } from '../../interfaces/item';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
state = State;
nom: string;
reference: string;
etat: string;
stateLibelles = Object.values(State);
newItem: Item;
  constructor() { }

  ngOnInit() {
    this.etat = State.ALIVRER;
    this.newItem={
      id: '',
      name: '',
      reference: '',
      state:State.ALIVRER

    }
  }
  process():void{
console.log(this.newItem);
this.reset();
  }
  reset(): void{
    this.newItem={
      id: '',
      name: '',
      reference: '',
      state:State.ALIVRER

    }

}
}
