import { Directive, Input, OnInit, HostBinding } from '@angular/core';
import { State } from '../../enums/state.enum';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnChanges {
  @Input() appState: State;
  @HostBinding('class') _elementClass: string;
  constructor() {
  }
  ngOnChanges() {
    this._elementClass = this.transformer(this.appState);
    console.log(this.transformer(State.LIVREE));

  }
  transformer(val: State): string {
    let res: string;
    res = "state-" + val.replace(" ", "").replace("é", "e").toLowerCase();

    return res;
  }

}
