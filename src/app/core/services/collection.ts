import { Item } from "../../shared/interfaces/item";
import { State } from "../../shared/enums/state.enum";

export const COLLECTION: Item[]=[
    {
        id: 'a1',
        name:'Christophe',
        reference: '1234',
        state: State.ALIVRER

    },
    {
        id: 'a2',
        name:'Yasmine',
        reference: '444',
        state: State.ENCOURS

    },
    {
        id: 'a3',
        name:'Kamel',
        reference: '555',
        state: State.LIVREE

    }
]